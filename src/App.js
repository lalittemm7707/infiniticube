import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Main from "./components/Main";

function App() {
  return (
    <div className="App">
      <section className="section">
        <div className="container">
          <article className="message">
            <div className="message-header is-centered has-text-centered">
              <p>IP: 3000/admin</p>
              <button className="delete" aria-label="delete"></button>
            </div>
            <div className="message-body">
              <Main />
            </div>
          </article>
        </div>
      </section>
    </div>
  );
}

export default App;
