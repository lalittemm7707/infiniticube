import React, { useState, useEffect } from "react";

const Tags = ({ onChange }) => {
  const [tags, setTags] = useState([]);
  const [input, setInput] = useState("");

  function handleDelete(deleteTag) {
    setTags(tags.filter(tag => tag !== deleteTag));
  }

  function handleAddition(tag) {
    setTags(state => [...tags, input]);
    setInput("");
  }
  useEffect(() => {
    onChange(tags);
  }, [tags]);
  return (
    <div className="box is-flex">
      <div className="field is-grouped is-grouped-multiline">
        {tags.map(tag => {
          return (
            <div className="control">
              <div className="tags has-addons">
                <a className="tag is-link">{tag}</a>
                <a
                  className="tag is-delete"
                  onClick={e => handleDelete(tag)}></a>
              </div>
            </div>
          );
        })}

        <input
          className="input"
          onChange={e => {
            setInput(e.target.value);
          }}
          style={{ width: "20%", borderColor: "transparent" }}
          onKeyDown={e => {
            console.log(e.keyCode);
            if (e.keyCode === 32 || e.keyCode === 13 || e.keyCode === 188) {
              handleAddition();
            } else if (e.keyCode === 8 && input.trim()=== "") {
              handleDelete(tags[tags.length - 1]);
            }
          }}
          value={input}
        />
      </div>
    </div>
  );
};

export default Tags;
