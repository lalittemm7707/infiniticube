import React, { useState, Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronCircleRight } from "@fortawesome/free-solid-svg-icons";
import Tags from "../Tags";
import { sourceIconList } from "../../util";
import { Link } from "react-router-dom";

const ConfigureSource = ({ selectedSource }) => {
  const [selectedTags, setSelectedTags] = useState([]);

  const handleChange = tag => {
    setSelectedTags(tag);
  };

  return (
    <Fragment>
      <div className="box">
        <article className="media">
          <div className="media-left">
            <FontAwesomeIcon icon={sourceIconList[selectedSource[0]]} />
            Username
          </div>
          <div className="media-content">
            <div className="content">
              <p>
                <Tags onChange={handleChange} />
              </p>
            </div>
          </div>
        </article>
        {selectedSource.length > 1 && (
          <Fragment>
            <FontAwesomeIcon icon={faChevronCircleRight} />
            {selectedSource[1]}
          </Fragment>
        )}
      </div>
      {selectedTags.length ? (
        <Link to="/marketer">
          <button className="button is-info" style={{ margin: "5%" }}>
            Start Ingeation
          </button>
        </Link>
      ) : null}
    </Fragment>
  );
};

export default ConfigureSource;
