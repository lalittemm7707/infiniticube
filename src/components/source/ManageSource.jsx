import React, { useState, Fragment, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { sourceIconList } from "../../util";
import ConfigureSource from "./ConfigureSource";
function ManageSource() {
  const [selectedSource, setSelectedSource] = useState([]);
  const sourceList = ["Facebook", "Twitter", "YouTube"];

  const addAndRemoveSource = (isChecked, value) => {
    if (isChecked) {
      setSelectedSource([...selectedSource, value]);
    } else {
      setSelectedSource(selectedSource.filter(source => source !== value));
    }
  };
  return (
    <Fragment>
      <div className="column is-3">
        <nav className="panel">
          <p className="panel-heading">Select Source</p>
          {sourceList.map(source => (
            <label className="panel-block">
              <input
                type="checkbox"
                value={source}
                onClick={e => {
                  addAndRemoveSource(e.target.checked, e.target.value);
                }}
              />
              <Fragment>
                <FontAwesomeIcon icon={sourceIconList[source]} />
              </Fragment>
              {source}
            </label>
          ))}
        </nav>
      </div>
      <div className="column is-1">
        <Fragment>
          {selectedSource.length ? (
            <FontAwesomeIcon icon={faChevronRight} />
          ) : null}
        </Fragment>
      </div>
      {selectedSource.length ? (
        <div className="column is-flex is-vcentered">
          <ConfigureSource selectedSource={selectedSource} />
        </div>
      ) : null}
    </Fragment>
  );
}

export default ManageSource;
