import React, { useState, useEffect, Fragment } from "react";
import { Bar, Line, Pie } from "react-chartjs-2";
import { useTable } from 'react-table'
import Axios from "axios";

function Marketer() {
  const [data, setData] = useState([]);
  const [users, setUser] = useState(null);
  const [uniqueUser, setUniqueUser] = useState({});
  const [ndata, setnData] = useState([]);

  useEffect(() => {
    Axios.get(
      "http://52.175.201.248:3000/facebook/facebook_post_comments/1"
    ).then(res => {
      setData(res.data.list);
    });
  }, []);

  useEffect(() => {
    const key = {};
    data.map(
      a => {
        key[a.username] = {
          data: !key[a.username] ? 0 : key[a.username].data + 1
        };
      },
    );
    setUniqueUser(key);
  }, [data]);

  useEffect(() => {
    const tempData = [];
    const label = [];
    Object.keys(uniqueUser).map(i => {
      tempData.push(uniqueUser[i].data);
      label.push(i);
    });
    setnData(tempData);
    setUser(label);
  }, [uniqueUser]);

  const getData = () => {
    return {
      labels: users,
      datasets: [
        {
          data: ndata,
          backgroundColor: [
            "#2ecc71",
            "#3498db",
            "#95a5a6",
            "#9b59b6",
            "#f1c40f",
            "#e74c3c",
            "#34495e"
          ],
        },
        
      ]
    };
  };

  return (
    <section className="section">
      {data.length ? (
        <Fragment>
          <div className="columns">
            <div className="column is-half">{data && <App _data={uniqueUser} />} </div>
            <div className="column is-half">
              <Bar data={getData()} />
            </div>
          </div>
          <div className="columns">
            <div className="column is-half">
              <Line data={getData()} /> </div>
            <div className="column"><Pie data={getData()} /> </div>
          </div>
        </Fragment>
      ) : null}
    </section>
  );
}


 
 function App({_data}) {
   const [uniqueUser,setUniqueUser] = useState([])
    const [data, setData] = useState([])
  
   useEffect(() => {
     const tempData = [];
     Object.keys(_data).map(user => {
      tempData.push({name:user, username: _data[user].data})
     })
     setData(tempData)
   }, [_data])
  
 
   const columns = React.useMemo(
    () => [
      {
        Header: 'Username',
        accessor: 'name', // accessor is the "key" in the data
      },
      {
        Header: 'Comment',
        accessor: 'username',
      },
    ],
    []
  )
 
   const {
     getTableProps,
     getTableBodyProps,
     headerGroups,
     rows,
     prepareRow,
   } = useTable({ columns, data })
 
   return (
     <table {...getTableProps()} style={{ border: 'solid 1px blue' }}>
       <thead>
         {headerGroups.map(headerGroup => (
           <tr {...headerGroup.getHeaderGroupProps()}>
             {headerGroup.headers.map(column => (
               <th
                 {...column.getHeaderProps()}
                 style={{
                   background: 'aliceblue',
                   color: 'black',
                   fontWeight: 'bold',
                 }}
               >
                 {column.render('Header')}
               </th>
             ))}
           </tr>
         ))}
       </thead>
       <tbody {...getTableBodyProps()}>
         {rows.map(row => {
           prepareRow(row)
           return (
             <tr {...row.getRowProps()}>
               {row.cells.map(cell => {
                 return (
                   <td
                     {...cell.getCellProps()}
                     style={{
                       padding: '10px',
                       border: 'solid 1px gray',
                     }}
                   >
                     {cell.render('Cell')}
                   </td>
                 )
               })}
             </tr>
           )
         })}
       </tbody>
     </table>
   )
 }


export default Marketer;
