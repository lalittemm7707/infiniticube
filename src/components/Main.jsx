import React, { useState, Fragment } from "react";
import ManageSource from "./source/ManageSource";

function Main() {
  const [selectedTab, setSelectedTab] = useState(0);
  return (
    <div>
      <div className="columns">
        <div className="column">
          <button
            className={`button is-dark ${selectedTab !== 1 && "is-outlined"}`}
            onClick={() => setSelectedTab(1)}>
            MANAGE SOURCE
          </button>
        </div>
        <div className="column">
          <button
            className={`button is-dark ${selectedTab !== 2 && "is-outlined"}`}
            onClick={() => setSelectedTab(2)}>
            MANAGE TEAM
          </button>
        </div>
      </div>
      <div className="columns is-vcentered">
        <Fragment>{switchScreens(selectedTab)}</Fragment>
      </div>
    </div>
  );
}

const switchScreens = selectedTab => {
  switch (selectedTab) {
    case 1:
      return <ManageSource />;
    default:
      return null;
  }
};

export default React.memo(Main);
