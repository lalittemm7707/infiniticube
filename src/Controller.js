import React from "react";
import App from "./App";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Marketer from "./components/Marketer";
import Main from "./components/Main";

function Controller() {
  return (
    <div className="App">
      <section className="section">
        <div className="container">
          <article className="message">
            <div className="message-header is-centered has-text-centered">
              <p>IP: 3000/admin</p>
              <button className="delete" aria-label="delete"></button>
            </div>
            <div className="message-body">
              <Router>
                <Switch>
                  <Route exact path="/">
                    <Main />
                  </Route>
                  <Route path="/admin">
                    <Main />
                  </Route>
                  <Route path="/marketer">
                    <Marketer />
                  </Route>
                </Switch>
              </Router>
            </div>
          </article>
        </div>
      </section>
    </div>
  );
}

export default Controller;
