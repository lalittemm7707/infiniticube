import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebook,
  faTwitter,
  faYoutube
} from "@fortawesome/free-brands-svg-icons";
import React from "react";
export const sourceIconList = {
  Facebook: faFacebook,
  Twitter: faTwitter,
  YouTube: faYoutube
};
